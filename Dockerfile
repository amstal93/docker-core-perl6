FROM buildpack-deps:focal-scm
MAINTAINER Konstantin Narkhov <konstantin@narkhov.pro>

ARG MAXMINDACCOUNT=0

ARG MAXMINDLICENSE=000000000000

ENV GEOIPUPD_VERSION     4.9.0
ENV RAKUDO_VERSION       2021.12

ENV PATH /usr/bin/:/usr/share/perl6/site/bin:/usr/share/perl6/vendor/bin:/usr/share/perl6/core/bin:/usr/local/bin:$PATH

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN --mount=type=bind,target=/resources,source=resources ls -la /resources && groupadd -r raku && useradd -r -g raku raku; \
	apt-get update && apt-get install -y --no-install-recommends \
		apt-utils \
		g++ \
		gcc \
		libc6-dev \
		make \
		pkg-config \
		sudo \
		cmake \
		mc \
		libencode-perl \
		xz-utils \
		autoconf \
		libltdl-dev \
		libpng-dev \
		libjpeg-dev \
		libcogl-pango-dev \
		libssl-dev \
		libfreetype6-dev \
		build-essential \
		libcurl4-openssl-dev \
		zlib1g-dev \
		vim \
		jq \
		socat \
		software-properties-common \
		bzip2 \
		uuid-dev \
	&& rm -rf /var/lib/apt/lists/*; \
	\
# install solidity
	add-apt-repository ppa:ethereum/ethereum -y; \
	apt-get update && apt-get install -y solc; \
	ln -s /usr/bin/solc /usr/local/bin/solc; \
    \
# install geoipupdate
	cd /; \
	curl -fsSL https://github.com/maxmind/geoipupdate/releases/download/v${GEOIPUPD_VERSION}/geoipupdate_${GEOIPUPD_VERSION}_linux_amd64.deb -o geoipupdate.deb; \
	apt-get install -y ./geoipupdate.deb; \
	geoipupdate -V; \
	\
# update databases with geoipupdate
	mkdir -p $HOME/geoipupdate/tmp && cd "$_"; \
	printf "AccountID $MAXMINDACCOUNT\nLicenseKey $MAXMINDLICENSE\nEditionIDs GeoLite2-Country GeoLite2-City\n" > $HOME/geoipupdate/tmp/GeoIP.conf; \
	geoipupdate -v -f $HOME/geoipupdate/tmp/GeoIP.conf; \
	rm -rf $HOME/geoipupdate/tmp; \
	ls -a /usr/share/GeoIP; \
	\
# install imagemagick
	mkdir $HOME/imagemagick; \
	cd $HOME/imagemagick; \
	curl -fsSL https://www.imagemagick.org/download/releases/ImageMagick-6.9.12-44.tar.xz -o ImageMagick-6.9.12-44.tar.xz; \
	tar -xpJf ImageMagick-6.9.12-44.tar.xz -C $HOME/imagemagick; \
	cd $HOME/imagemagick/ImageMagick*; \
	./configure -with-perl; \
	make; \
	make install; \
	ldconfig /usr/local/lib; \
	\
# install rakudo
	rm -rf /var/lib/apt/lists/*; \
	mkdir $HOME/rakudo; \
#	curl -fsSL https://rakudo.org/latest/star/src -o rakudo.tar.gz; \
#	curl -fsSL https://rakudo.org/dl/star/rakudo-star-$RAKUDO_VERSION.tar.gz -o rakudo.tar.gz; \
	tar xzf /resources/rakudo-star-$RAKUDO_VERSION.tar.gz -C $HOME/rakudo; \
	cd $HOME/rakudo/rakudo-star-$RAKUDO_VERSION; \
    bin/rstar install -p /usr; \
    export PATH=/usr/bin/:/usr/share/perl6/site/bin:/usr/share/perl6/vendor/bin:/usr/share/perl6/core/bin:$PATH; \
	\
# update zef
	zef update; \
	zef --debug install git://github.com/ugexe/zef.git; \
	zef --version; \
	\
# install Raku modules
	zef install Data::Dump; \
	zef install XML; \
	zef install GeoIP2; \
	zef install URI::Encode; \
	zef install Node::Ethereum::Keccak256::Native; \
	zef install Net::Ethereum; \
	zef install LZW::Revolunet; \
	zef install Compress::Bzip2; \
	zef install Compress::Zlib; \
	zef install FastCGI::NativeCall; \
	zef install FastCGI::NativeCall::Async; \
	zef --force install Router::Right; \
	zef install https://github.com/pheix/html-template.git; \
    zef install https://github.com/pheix/perl6-magickwand.git; \
	\
# remove folders and tgz
	cd /; \
	rm -rf /rakudo.tar.gz $HOME/rakudo; \
	rm -rf /ImageMagick-6.9.9-51.tar.xz $HOME/imagemagick; \
	rm -rf /geoipupdate.tar.gz $HOME/geoipupdate; \
	rm -rf $HOME/git; \
	apt-get purge -y make cmake gcc g++ pkg-config autoconf build-essential; \
#	dpkg -l | grep -E "\-dev\s" | grep -v "ssl" | awk '{print $2}' | xargs -d '\n' -- apt-get purge -y $1; \
	dpkg -l | grep -E "\-dev\s" | grep -v "ssl" | awk '{print $2}' | xargs -d '\n' -- apt-get purge -y; \
	apt-get clean; \
	rm -rf /var/lib/apt/lists/*; \
	rm -f /geoipupdate.deb; \
	\
# test versions
	jq --version; \
	vi --version; \
	convert --version; \
	raku --version; \
	/usr/local/bin/solc --version; \
	geoipupdate -V;

CMD ["/bin/bash"]
